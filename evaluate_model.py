#!/usr/bin/env python

from __future__ import print_function

import time
start_time = time.time()

from keras.models import load_model
from keras.utils import np_utils
from keras.utils import to_categorical
import tensorflow as tf
from tensorflow import keras
import uproot as uprt
import h5py
import os
import json
import fnmatch
import ROOT
import numpy
import root_numpy
import pandas
import csv

import DNNClass as DNN

def KILL(log):
    raise SystemExit('\n '+'\033[1m'+'@@@ '+'\033[91m'+'FATAL'  +'\033[0m'+' -- '+log+'\n')

from optparse import OptionParser

def parse_arguments():
    usage = """ 
    usage: %prog [options] 
    """
    parser = OptionParser(usage=usage)
    parser.add_option('-i', '--input', dest='input', action='store', default='merged_ttHbb_signal.root',help='path to input .root file')
    parser.add_option('-o', '--output', dest='output', action='store', default='out.root',help='path to output .root file')                            
    parser.add_option('-j', '--json-conf', dest='json_conf', action='store', default='config.json',help='path to .json file with input/output configuration')
    (options, args) = parser.parse_args()
    return options, args


def convert_to_list_of_structured_numpy_arrays(np_array):

    if np_array.dtype.names is None: return [np_array]
    if np_array.dtype.fields is None: return [np_array]

    _reformat_nparray = False
    for i_name in np_array.dtype.names:
        if np_array.dtype.fields[i_name][0] == numpy.dtype('O'):
           _reformat_nparray = True; break;

    if not _reformat_nparray: return [np_array]

    _output_npas = []

    for i_name in sorted(np_array.dtype.names):

        _i_npa = None

        if np_array.dtype.fields[i_name][0] == numpy.dtype('O'):

           _i_lists = []

           _size_per_event = None

           for _tmp in np_array[i_name]:

               _tmp_list = _tmp.tolist()

               if _size_per_event == None:
                  _size_per_event = len(_tmp_list)

               elif len(_tmp_list) != _size_per_event:

                  _log_msg  = 'input numpy array has non-constant length '
                  _log_msg += '(found size='+str(len(_tmp_list))
                  _log_msg += ', expected size='+str(_size_per_event)+')'

                  KILL('convert -- '+_log_msg+', unsupported format for TTree branches')

               if _size_per_event == 1: _i_lists += [_tmp_list]
               else: _i_lists += [(_tmp_list,)]

           _i_npa = numpy.array(_i_lists)

           if _size_per_event not in (1, None):

              if len(_i_npa.shape) != 3:
                 log_msg = log_prx+'convert_to_list_of_structured_numpy_arrays --'
                 log_msg += 'input field "'+str(i_name)+'" has size='+str(_size_per_event)
                 log_msg += ', but its numpy.array has len(shape) different from 3: '+str(_i_npa.shape)
                 KILL(log_msg)

              if _i_npa.shape[2] != _size_per_event:
                 log_msg = log_prx+'convert_to_list_of_structured_numpy_arrays --'
                 log_msg += 'input field "'+str(i_name)+'" has size='+str(_size_per_event)
                 log_msg += ', but its numpy.array has shape[2]='+str(_i_npa.shape[2])
                 KILL(log_msg)

           if len(_i_npa.shape) <= 1:
              _i_npa = numpy.array(_i_lists, dtype=[(i_name, _i_npa.dtype)])
           else:
              _i_npa_shape_size = _i_npa.shape[2] if (len(_i_npa.shape) == 3) else _size_per_event
              _i_npa = numpy.array(_i_lists, dtype=[(i_name, _i_npa.dtype, (_i_npa_shape_size,))])

        else:

           _i_npa = np_array[i_name]

           if len(_i_npa.shape) <= 1:
              _i_npa = numpy.array(_i_npa, dtype=[(i_name, _i_npa.dtype)])

           else:
              if len(_i_npa.shape) != 3:
                 log_msg = log_prx+'convert_to_list_of_structured_numpy_arrays --'
                 log_msg += 'numpy.array associated to input field "'+str(i_name)+'" has len(shape) different from 3: '+str(_i_npa.shape)
                 KILL(log_msg)

              _i_npa = numpy.array(_i_npa, dtype=[(i_name, _i_npa.dtype, (_i_npa.shape[2],))])

        if _i_npa is None:
           KILL(log_prx+'convert_to_list_of_structured_numpy_arrays -- failed to construct numpy.array object for output list (name="'+i_name+'")')

        _output_npas += [_i_npa]

    return _output_npas



def prepare_data(sample):

    DataFrames = {} # define empty dictionary to hold dataframes
            
    ML_inputs = ["MEM","blr","N_btags_Loose","mass_tag_tag_min_deltaR","mass_tag_tag_max_mass","mass_jet_tag_tag_max_pT",
        "pT_tag_tag_min_deltaR","avgDeltaR_tag_tag","avgDeltaR_jet_jet","maxDeltaEta_tag_tag","maxDeltaEta_jet_jet",
        "H2_tag","H4_tag","R2_jet","centrality_tags","sphericity_tag","jet4_btag","pT_jet_tag_min_deltaR",
        "multiplicity_higgsLikeDijet15"] # list of features for ML model
    
    file = uprt.open(sample)
    tree = file["liteTreeTTH_step7_cate9"]
    DataFrames[sample] = tree.pandas.df(ML_inputs)

    norm = pandas.DataFrame(index=['mu', 'std'], columns=ML_inputs)

    with open('../CMSSW_10_2_15/src/tth-trf/For_evaluation/Eval_framework/dracoevaluation/4j3t/checkpoints/variable_norm.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for idx, row in enumerate(csv_reader):
            if idx != 0:
               norm[row[0]]['mu'] = row[1]
               norm[row[0]]['std'] = row[2]

    norm[ML_inputs] = norm[ML_inputs].astype(float)

    print(DataFrames[sample])    
    DataFrames[sample][ML_inputs] = (DataFrames[sample][ML_inputs] - norm[ML_inputs].iloc[0])/norm[ML_inputs].iloc[1]
    print(DataFrames[sample][ML_inputs])

    return DataFrames[sample][ML_inputs]


def main(options, paths):

    #model = load_model(options.model)
    #model.summary()
    #print( model.predict( prepare_data(options.input).values ) )

    ROOT.gROOT.SetBatch()

    log_prx = os.path.basename(__file__)+' -- '

    ### args validation ---
    #if opts_unknown:
    #   KILL(log_prx+'unsupported command-line arguments: '+str(opts_unknown))

    if not os.path.isfile(options.input):
       KILL(log_prx+'target path to input .root file is not valid [-i]: '+str(options.input))

    if os.path.exists(options.output):
       KILL(log_prx+'target path to output .root file already exists [-o]: '+str(options.output))

    if not os.path.isfile(options.json_conf):
       KILL(log_prx+'target path to .json file with input/output configuration is not valid [-j]: '+str(options.json_conf))

    conf_list = json.load(open(options.json_conf))

    input_tfile = ROOT.TFile.Open(options.input)
    if (not input_tfile) or input_tfile.IsZombie() or input_tfile.TestBit(ROOT.TFile.kRecovered): raise SystemExit(1)

    output_nparrays_dict = {}

    for conf_dict in conf_list:

        if not isinstance(conf_dict, dict):
           KILL(log_prx+'entry of .json configuration file is not a python dictionary: '+str(conf_dict))

        if 'TTree_input_names' not in conf_dict:
           KILL(log_prx+'configuration entry dictionary does not contain field "'+'TTree_input_names'+'" (check .json configuration file)')

        if 'TTree_output_name' not in conf_dict:
           KILL(log_prx+'configuration entry dictionary does not contain field "'+'TTree_output_name'+'" (check .json configuration file)')

        if 'model_path' not in conf_dict:
           KILL(log_prx+'configuration entry dictionary does not contain field "'+'model_path'+'" (check .json configuration file)')

        if 'model_output_branches' not in conf_dict:
           KILL(log_prx+'configuration entry dictionary does not contain field "'+'model_output_branches'+'" (check .json configuration file)')

        if 'copy_input_branches' not in conf_dict:
           KILL(log_prx+'configuration entry dictionary does not contain field "'+'copy_input_branches'+'" (check .json configuration file)')

        # TChain of input trees in input file
        i_tree_input_names = conf_dict['TTree_input_names']

        i_tchain = ROOT.TChain()

        for ij_tree_name in i_tree_input_names:

            if i_tchain.Add(str(options.input)+'/'+str(ij_tree_name)) != 1:
               KILL(log_prx+'failed to append target TObject in input .root file to TChain: '+str(options.input)+':'+str(ij_tree_name))

        # name of output TTree
        i_tree_name_output = conf_dict['TTree_output_name']

        if i_tree_name_output in output_nparrays_dict:
           KILL(log_prx+'key "'+str(i_tree_name_output)+'" for output TTree already exists in output TTree dictionary (check .json configuration file)')

        i_tree_branches = []
        for i_branch in i_tchain.GetListOfBranches(): i_tree_branches += [i_branch.GetName()]

        # list of input branches to be copied (accepts wildcard-matches via fnmatch)
        COPY_INPUT_BRANCHES = []
        for _tmp_bra in i_tree_branches:

            keep_branch = bool(_tmp_bra in conf_dict['copy_input_branches'])

            # if branch is not an exact match, check input directives for wildcard-based match
            if not keep_branch:
               for _tmp_inp in conf_dict['copy_input_branches']:
                   if fnmatch.fnmatch(_tmp_bra, _tmp_inp):
                      keep_branch = True; break;

            if keep_branch:
               COPY_INPUT_BRANCHES += [_tmp_bra]

        COPY_INPUT_BRANCHES = list(set(COPY_INPUT_BRANCHES))

        input_variables = []

        # read csv file and save input variable variable list
        with open(conf_dict['model_path']+'/variable_norm.csv') as csv_file:
             csv_reader = csv.reader(csv_file, delimiter=',')
             for idx, row in enumerate(csv_reader):
                 if idx != 0:
                     input_variables.append(row[0])

        # enable only necessary branches of input TTree
        i_tchain.SetBranchStatus('*', False)
        for i_branch in list(set(input_variables + COPY_INPUT_BRANCHES)):
            i_tchain.SetBranchStatus(i_branch, True)

        # DataFrame with the content of the model's inputs
        i_tchain_entries_nparray = root_numpy.tree2array(i_tchain, branches = input_variables)
        model_inputs_df = pandas.DataFrame(i_tchain_entries_nparray)

        # numpy array for model outputs
        model_output_branches_nparray = DNN.DNN_outputs_nparray(
          dataFrame = model_inputs_df,
          model     = conf_dict['model_path'],
          inputs    = input_variables,
          outputs   = conf_dict['model_output_branches'],
        )

        # numpy array for inputs to be copied in output file
        copy_input_branches_nparray = root_numpy.tree2array(i_tchain, branches=COPY_INPUT_BRANCHES)


        for i_model_output_branch_name in model_output_branches_nparray.dtype.names:
            if i_model_output_branch_name in copy_input_branches_nparray.dtype.names:
               KILL(log_prx+'name of new model-output branch "'+i_model_output_branch_name+'" already exists in "copy_input_branches" (check .json configuration file)')

        output_nparrays_dict[i_tree_name_output]  = convert_to_list_of_structured_numpy_arrays(model_output_branches_nparray)
        output_nparrays_dict[i_tree_name_output] += convert_to_list_of_structured_numpy_arrays(copy_input_branches_nparray)

    input_tfile.Close()
    ### -------------------

    ### output
    output_tfile = ROOT.TFile(options.output, 'recreate')
    if (not output_tfile) or output_tfile.IsZombie(): raise SystemExit(1)

    for i_tree_name_output in sorted(output_nparrays_dict.keys()):

        output_tfile.cd()

        i_output_tree = None
        for j_nparray in output_nparrays_dict[i_tree_name_output]:

            if i_output_tree == None: i_output_tree = root_numpy.array2tree(j_nparray, name=i_tree_name_output)
            else: i_output_tree = root_numpy.array2tree(j_nparray, tree=i_output_tree)

        i_output_tree.Write('', ROOT.TObject.kOverwrite)

        print('\033[1m'+'\033[93m'+'> TTree:'+'\033[0m', i_tree_name_output)

    output_tfile.Close()

    print('\033[1m'+'\033[92m'+'[output]'+'\033[0m', os.path.relpath(options.output))
    ### -------------------

    print(('\033[1m'+'> Time Elapsed = {:.3f} sec'+'\033[0m').format((time.time()-start_time)))



if __name__ == '__main__':
    options, paths = parse_arguments()
    main(options = options, paths = paths)
